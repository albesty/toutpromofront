/*export class UserInfo {
    name: string;
    username: string;
    email: string;
    password: string;
    role: string[] = ['admin'];

    // tslint:disable-next-line:no-misused-new
    constructor( name: string, username: string, email: string, password: string, role: string[]) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
}
}*/

export interface UserInfo {
    name: string;
    username: string;
    email: string;
    password: string;
    joinedAt: Date;
}
