import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../app-auth/services/auth.service';
import {AlertService} from '../../app-alert/services/alert.service';
import {first} from 'rxjs/operators';
import {UserInfo} from '../../app-auth/models/user-info';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
   user: UserInfo;
  constructor(private fb: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private alertService: AlertService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      usernameOrEmail: ['', Validators.required],
      password: ['', Validators.required]

    });
    this.authService.logout();
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.authService.login(this.f.usernameOrEmail.value, this.f.password.value)
        .pipe(first())
        .subscribe(
            data => {
              this.router.navigate(['/']);
            },
            error => {
              this.alertService.error(error);
            });

  }
}
