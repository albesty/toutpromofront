import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NzTabSetComponent} from 'ng-zorro-antd';
import {UserInfo} from '../../app-auth/models/user-info';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../app-auth/services/auth.service';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
 /* @ViewChild(NzTabSetComponent)
  tabSet: NzTabSetComponent;
*/
  username: string;
  userInfo: UserInfo = {} as UserInfo;
  private sub: Subscription;
  private tabSet: any;

  constructor(private route: ActivatedRoute,
              private authService: AuthService) {

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.username = params.username;
      this.loadUserProfile(this.username);
    });
  }

  loadUserProfile(username: string) {
    this.tabSet.tabs[0].active = true;
    this.authService.getUserProfile(username)
        .pipe(first())
        .subscribe( res => {
              this.userInfo = res;
            },
            error => {
              console.log(error);
            });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
