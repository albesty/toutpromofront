import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../app-auth/services/auth.service';
import {AlertService} from '../../app-alert/services/alert.service';
import {first, map} from 'rxjs/operators';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {


  registerForm: FormGroup;

  constructor(
      private formBuilder: FormBuilder,
     /* private toastr: ToastrService,*/
      private router: Router,
      private authService: AuthService,
      private alertService: AlertService
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      username: [
        '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(15)],
        this.validateUsernameAvailability.bind(this)
      ],
      email: [
        '',
        [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')],
         this.validateEmailNotTaken.bind(this)
      ],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onFormSubmit() {
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.authService.register(this.registerForm.value)
        .pipe(first())
        .subscribe(
            data => {
              this.router.navigate(['/demarrer/login']);
            },
            error => {
              this.alertService.error(error || 'Sorry! Something went wrong. Please try again!');
            });
  }


  validateUsernameAvailability(control: FormControl) {
    return this.authService.checkUsernameAvailability(control.value).pipe(map(res => {
      return res.available ? null : { usernameTaken: true };
    }));
  }

    validateEmailNotTaken(control: AbstractControl) {
      return this.authService.checkEmailAvailability(control.value).pipe(map(res => {
        return res.available ? null : { emailTaken: true };
      }));
    }

/*  validateUsernameAvailability(control: FormControl) {
    const q = new Promise((resolve, reject) => {
      setTimeout(() => {
        this.authService.checkUsernameAvailability(control.value).subscribe(() => {
          resolve(null);
        }, () => { resolve({ usernameTaken: true }); });
      }, 1000);
    });
    return q;
  }*/

}
