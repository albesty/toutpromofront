import {AfterViewInit, Component, Inject, OnInit, Renderer2} from '@angular/core';

import {DOCUMENT} from '@angular/common';

declare function welcomeAnimate(): any;

@Component({
  selector: 'app-demarrer',
  templateUrl: './demarrer.component.html',
  styleUrls: ['./demarrer.component.scss']
})
export class DemarrerComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }


}
