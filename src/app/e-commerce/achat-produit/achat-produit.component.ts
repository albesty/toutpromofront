import {AfterViewInit, Component, Directive, ElementRef, HostListener, Inject, OnInit, Renderer2, ViewChild} from '@angular/core';
import {DialogService, MenuItem} from 'primeng/api';
import {NzModalService} from 'ng-zorro-antd';
import {NewProduitComponent} from '../modals/new-produit/new-produit.component';
import {first} from 'rxjs/operators';
import {EcommerceService} from '../services/ecommerce.service';
import { ViewEncapsulation } from '@angular/core';
import {fromArray} from 'rxjs/internal/observable/fromArray';
import {DOCUMENT} from '@angular/common';

declare function carouselControls(): any;


@Component({
  selector: 'app-achat-produit',
  templateUrl: './achat-produit.component.html',
  styleUrls: ['./achat-produit.component.scss'],
  providers: [DialogService]

})
export class AchatProduitComponent implements OnInit, AfterViewInit {
    carouselTable: any[];
    isprev = false;
    isnext = true;
    show = false;
    percent = 100;
    promoProds: any;
    totalElements: number;
    totalPage: number;
    currentPage = 0;
    pageSize = 5;
    gridStyle = {
        width: '25%',
        textAlign: 'center'
    };
    activeSildeIndex: number;

    carouselSize(array, size) {
        let results = [];
        results = [];
        while (array.length) {
            results.push(array.splice(0, size));
        }
        return results;
    }

    constructor(@Inject(DOCUMENT) private document: Document,
                private ecommerceService: EcommerceService, private modalService: NzModalService,
                public dialogService: DialogService,
                private render: Renderer2) {
    }

    ngOnInit() {
        carouselControls();

        this.ecommerceService.getAllProdByEspace(5).subscribe(
            data => {
                this.promoProds = this.carouselSize(data, 5);
                const last = this.promoProds[this.promoProds.length - 1].length;
                if (this.promoProds.length > 1 && last < 5) {
                    this.promoProds[this.promoProds.length - 1] = [...this.promoProds[this.promoProds.length - 1], ...this.promoProds[0].slice(0, 5 - last)];
                }
                console.log(this.promoProds);
            },
            error1 => {
                console.log(error1);
            }
        );

    }

    openProduitModal() {
        const ref = this.dialogService.open(NewProduitComponent, {
            header: 'Nouveau produit',
            contentStyle: {overflow: 'auto'}
        });
    }

    ngAfterViewInit() {
   /*     this.render.setStyle(this.prev.nativeElement, 'visibility', 'hidden');
        Array.from(this.document.getElementsByClassName('carousel-item')).forEach(function(item1, index1) {
            console.log(index1);
        });*/

    }

}
