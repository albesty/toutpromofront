import { Component, OnInit } from '@angular/core';
import {DialogService} from "primeng/api";
import {NewProduitComponent} from "../modals/new-produit/new-produit.component";
import {EcommerceService} from '../services/ecommerce.service';



@Component({
  selector: 'app-vote-prix',
  templateUrl: './vote-prix.component.html',
  styleUrls: ['./vote-prix.component.scss'],
  providers: [DialogService]
})
export class VotePrixComponent implements OnInit {
  percent=100;
  dynamic: number;
  type: string;
  voteProds: any;

  voteCarouselSize(array, size) {
    let results = [];
    results = [];
    while (array.length) {
      results.push(array.splice(0, size));
    }
    return results;
  }


  constructor(public dialogService:DialogService, private ecommerceService: EcommerceService) { }

  ngOnInit() {

    this.ecommerceService.getAllProdByEspace(4).subscribe(
        data => {
          this.voteProds = this.voteCarouselSize(data, 5);
          let last = this.voteProds[this.voteProds.length - 1].length;
          if (this.voteProds.length > 1 && last < 5) {
            this.voteProds[this.voteProds.length - 1] = [...this.voteProds[this.voteProds.length - 1], ...this.voteProds[0].slice(0, 5 - last)];
          }
          console.log(this.voteProds);
        },
        error1 => {
          console.log(error1);
        }
    );
  }

  increase(): void {
    this.percent = this.percent + 10;
    if (this.percent > 100) {
      this.percent = 100;
    }
  }
  decline(): void {
    this.percent = this.percent - 10;
    if (this.percent < 0) {
      this.percent = 0;
    }
  }


  showProduitModal() {
    const ref = this.dialogService.open(NewProduitComponent, {
      header: 'Nouveau produit',
      contentStyle: {"overflow": "auto"}
    });
  }

    openProduitModal() {

    }
}
