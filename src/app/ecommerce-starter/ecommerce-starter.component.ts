import { Component, OnInit } from '@angular/core';
import {EcommerceService} from '../e-commerce/services/ecommerce.service';

@Component({
  selector: 'app-ecommerce-starter',
  templateUrl: './ecommerce-starter.component.html',
  styleUrls: ['./ecommerce-starter.component.scss']
})
export class EcommerceStarterComponent implements OnInit {
    ecomStarterProds: any;
  ecomCarouselSize(array, size) {
    let results = [];
    results = [];
    while (array.length) {
      results.push(array.splice(0, size));
    }
    return results;
  }

  constructor(private ecommerceService: EcommerceService) { }

  ngOnInit() {

    this.ecommerceService.getAllProdByEspace(1).subscribe(
        data => {
          this.ecomStarterProds = this.ecomCarouselSize(data, 5);
          const last = this.ecomStarterProds[this.ecomStarterProds.length - 1].length;
          if (this.ecomStarterProds.length > 1 && last < 5) {
            this.ecomStarterProds[this.ecomStarterProds.length - 1] = [...this.ecomStarterProds[this.ecomStarterProds.length - 1], ...this.ecomStarterProds[0].slice(0, 5 - last)];
          }
          console.log(this.ecomStarterProds);
        },
        error1 => {
          console.log(error1);
        }
    );

  }

}
