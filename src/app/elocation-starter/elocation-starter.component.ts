import { Component, OnInit } from '@angular/core';
import {EcommerceService} from '../e-commerce/services/ecommerce.service';

@Component({
  selector: 'app-elocation-starter',
  templateUrl: './elocation-starter.component.html',
  styleUrls: ['./elocation-starter.component.scss']
})
export class ElocationStarterComponent implements OnInit {
    elocStarterProds: any;
  elocCarouselSize(array, size) {
    let results = [];
    results = [];
    while (array.length) {
      results.push(array.splice(0, size));
    }
    return results;
  }

  constructor(private ecommerceService: EcommerceService) { }

  ngOnInit() {
    this.ecommerceService.getAllProdByEspace(2).subscribe(
        data => {
          this.elocStarterProds = this.elocCarouselSize(data, 5);
          const last = this.elocStarterProds[this.elocStarterProds.length - 1].length;
          if (this.elocStarterProds.length > 1 && last < 5) {
            this.elocStarterProds[this.elocStarterProds.length - 1] = [...this.elocStarterProds[this.elocStarterProds.length - 1], ...this.elocStarterProds[0].slice(0, 5 - last)];
          }
          console.log(this.elocStarterProds);
        },
        error1 => {
          console.log(error1);
        }
    );
  }

}
