import {AfterViewInit, Component, Inject, OnInit, Renderer2} from '@angular/core';
import {DOCUMENT} from '@angular/common';

declare function init(): any;

@Component({
  selector: 'app-welcome-animate',
  templateUrl: './welcome-animate.component.html',
  styleUrls: ['./welcome-animate.component.scss']
})
export class WelcomeAnimateComponent implements OnInit, AfterViewInit {

  constructor(private renderer: Renderer2, @Inject(DOCUMENT) private document) {
  }

  ngOnInit() {
    init();
  }

  ngAfterViewInit() {
  }

}
