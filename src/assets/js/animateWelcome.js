(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"animateWelcome_atlas_", frames: [[581,850,32,40],[725,984,39,65],[347,844,35,27],[826,252,24,27],[200,749,34,41],[108,939,28,93],[74,911,32,93],[814,453,75,149],[231,503,70,149],[891,453,75,149],[154,503,75,149],[0,503,75,149],[578,414,75,149],[303,503,70,149],[737,365,75,149],[375,503,53,149],[990,794,32,93],[347,879,45,93],[487,850,45,93],[240,697,48,93],[290,697,48,93],[275,939,28,93],[138,939,28,93],[590,660,48,93],[421,689,48,93],[134,844,45,93],[300,844,45,93],[534,850,45,93],[823,699,48,93],[540,660,48,93],[655,516,68,93],[725,516,68,93],[513,565,68,93],[583,565,68,93],[973,699,48,93],[371,654,48,93],[975,604,45,93],[181,887,45,93],[440,850,45,93],[228,887,45,93],[671,868,45,93],[773,699,48,93],[490,660,48,93],[50,749,48,93],[690,706,48,93],[150,749,48,93],[571,755,48,93],[394,879,35,93],[340,749,48,93],[471,755,48,93],[100,749,48,93],[0,749,48,93],[640,706,48,93],[521,755,48,93],[665,963,28,93],[395,974,28,93],[168,982,28,93],[335,974,28,93],[0,911,35,93],[198,982,28,93],[431,945,28,93],[812,889,45,93],[859,889,45,93],[906,889,45,93],[718,889,45,93],[953,889,45,93],[581,896,45,93],[765,889,45,93],[365,974,28,93],[628,896,35,93],[228,982,28,93],[695,984,28,93],[305,939,28,93],[461,945,28,93],[491,945,28,93],[521,945,28,93],[551,945,28,93],[390,784,48,93],[621,801,48,93],[890,794,48,93],[37,911,35,93],[250,792,48,93],[790,794,48,93],[940,794,48,93],[740,794,48,93],[840,794,48,93],[200,792,48,93],[795,604,58,93],[0,654,58,93],[60,654,58,93],[855,604,58,93],[873,699,48,93],[180,654,58,93],[713,611,58,93],[915,604,58,93],[120,654,58,93],[430,594,58,93],[653,611,58,93],[0,214,658,198],[660,214,81,149],[826,302,75,149],[945,151,75,149],[923,699,48,93],[967,0,53,149],[743,214,81,149],[968,453,53,149],[903,302,75,149],[857,0,108,149],[660,365,75,149],[0,0,855,212],[857,151,86,149],[980,302,42,149],[77,503,75,149],[240,654,129,41],[0,414,576,87],[430,503,81,89],[0,844,65,65],[740,706,30,9],[300,792,35,45],[67,844,65,65],[826,214,28,36],[513,503,34,50],[671,801,65,65]]}
];


// symbols:



(lib.bag = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.blackphone = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.bluecar = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.bluehouse = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.bot = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_1 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_10 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_100 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_101 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_102 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_103 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_104 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_105 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_106 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_107 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_108 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_11 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_12 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_13 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_14 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_15 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_16 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_17 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_18 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_19 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_2 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(25);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_20 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(26);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_21 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(27);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_22 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(28);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_23 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(29);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_24 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(30);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_25 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(31);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_26 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(32);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_27 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(33);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_28 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(34);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_29 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(35);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_3 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(36);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_30 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(37);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_31 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(38);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_32 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(39);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_33 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(40);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_34 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(41);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_35 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(42);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_36 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(43);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_37 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(44);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_38 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(45);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_39 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(46);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_4 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(47);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_40 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(48);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_41 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(49);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_42 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(50);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_43 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(51);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_44 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(52);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_45 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(53);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_46 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(54);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_47 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(55);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_48 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(56);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_49 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(57);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_5 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(58);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_50 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(59);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_51 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(60);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_52 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(61);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_53 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(62);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_54 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(63);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_55 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(64);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_56 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(65);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_57 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(66);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_58 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(67);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_59 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(68);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_6 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(69);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_60 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(70);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_61 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(71);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_62 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(72);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_63 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(73);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_64 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(74);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_65 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(75);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_66 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(76);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_67 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(77);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_68 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(78);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_69 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(79);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_7 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(80);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_70 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(81);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_71 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(82);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_72 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(83);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_73 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(84);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_74 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(85);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_75 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(86);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_76 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(87);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_77 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(88);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_78 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(89);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_79 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(90);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_8 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(91);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_80 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(92);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_81 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(93);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_82 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(94);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_83 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(95);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_84 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(96);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_85 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(97);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_86 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(98);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_87 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(99);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_88 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(100);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_89 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(101);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_9 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(102);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_90 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(103);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_91 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(104);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_92 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(105);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_93 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(106);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_94 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(107);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_95 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(108);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_96 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(109);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_97 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(110);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_98 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(111);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_99 = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(112);
}).prototype = p = new cjs.Sprite();



(lib.car = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(113);
}).prototype = p = new cjs.Sprite();



(lib.fondslogan = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(114);
}).prototype = p = new cjs.Sprite();



(lib.house = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(115);
}).prototype = p = new cjs.Sprite();



(lib.player = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(116);
}).prototype = p = new cjs.Sprite();



(lib.sandal = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(117);
}).prototype = p = new cjs.Sprite();



(lib.shirt = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(118);
}).prototype = p = new cjs.Sprite();



(lib.shoppingcart = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(119);
}).prototype = p = new cjs.Sprite();



(lib.tshirt = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(120);
}).prototype = p = new cjs.Sprite();



(lib.trousers = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(121);
}).prototype = p = new cjs.Sprite();



(lib.yellowphone = function() {
	this.initialize(ss["animateWelcome_atlas_"]);
	this.gotoAndStop(122);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.textebienvenue1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.CachedTexturedBitmap_108();
	this.instance.parent = this;
	this.instance.setTransform(375.5,12,0.5,0.5);

	this.instance_1 = new lib.CachedTexturedBitmap_107();
	this.instance_1.parent = this;
	this.instance_1.setTransform(342.15,12,0.5,0.5);

	this.instance_2 = new lib.CachedTexturedBitmap_106();
	this.instance_2.parent = this;
	this.instance_2.setTransform(311.6,12,0.5,0.5);

	this.instance_3 = new lib.CachedTexturedBitmap_105();
	this.instance_3.parent = this;
	this.instance_3.setTransform(261.6,12,0.5,0.5);

	this.instance_4 = new lib.CachedTexturedBitmap_104();
	this.instance_4.parent = this;
	this.instance_4.setTransform(228.25,12,0.5,0.5);

	this.instance_5 = new lib.CachedTexturedBitmap_103();
	this.instance_5.parent = this;
	this.instance_5.setTransform(194.9,12,0.5,0.5);

	this.instance_6 = new lib.CachedTexturedBitmap_102();
	this.instance_6.parent = this;
	this.instance_6.setTransform(161.55,12,0.5,0.5);

	this.instance_7 = new lib.CachedTexturedBitmap_101();
	this.instance_7.parent = this;
	this.instance_7.setTransform(132.3,12,0.5,0.5);

	this.instance_8 = new lib.CachedTexturedBitmap_100();
	this.instance_8.parent = this;
	this.instance_8.setTransform(100.3,12,0.5,0.5);

	this.instance_9 = new lib.CachedTexturedBitmap_99();
	this.instance_9.parent = this;
	this.instance_9.setTransform(66.95,12,0.5,0.5);

	this.instance_10 = new lib.CachedTexturedBitmap_98();
	this.instance_10.parent = this;
	this.instance_10.setTransform(50.3,12,0.5,0.5);

	this.instance_11 = new lib.CachedTexturedBitmap_97();
	this.instance_11.parent = this;
	this.instance_11.setTransform(11.4,12,0.5,0.5);

	this.instance_12 = new lib.CachedTexturedBitmap_96();
	this.instance_12.parent = this;
	this.instance_12.setTransform(-10.3,20.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.textebienvenue1, new cjs.Rectangle(-10.3,12,427.5,114.1), null);


(lib.textetoutpromo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.CachedTexturedBitmap_95();
	this.instance.parent = this;
	this.instance.setTransform(283.95,6.5,0.5,0.5);

	this.instance_1 = new lib.CachedTexturedBitmap_94();
	this.instance_1.parent = this;
	this.instance_1.setTransform(233.95,6.5,0.5,0.5);

	this.instance_2 = new lib.CachedTexturedBitmap_93();
	this.instance_2.parent = this;
	this.instance_2.setTransform(200.6,6.5,0.5,0.5);

	this.instance_3 = new lib.CachedTexturedBitmap_92();
	this.instance_3.parent = this;
	this.instance_3.setTransform(177.6,6.5,0.5,0.5);

	this.instance_4 = new lib.CachedTexturedBitmap_91();
	this.instance_4.parent = this;
	this.instance_4.setTransform(141.5,6.5,0.5,0.5);

	this.instance_5 = new lib.CachedTexturedBitmap_90();
	this.instance_5.parent = this;
	this.instance_5.setTransform(119.3,6.5,0.5,0.5);

	this.instance_6 = new lib.CachedTexturedBitmap_89();
	this.instance_6.parent = this;
	this.instance_6.setTransform(85.95,6.5,0.5,0.5);

	this.instance_7 = new lib.CachedTexturedBitmap_88();
	this.instance_7.parent = this;
	this.instance_7.setTransform(52.6,6.5,0.5,0.5);

	this.instance_8 = new lib.CachedTexturedBitmap_87();
	this.instance_8.parent = this;
	this.instance_8.setTransform(19.9,6.5,0.5,0.5);

	this.instance_9 = new lib.CachedTexturedBitmap_86();
	this.instance_9.parent = this;
	this.instance_9.setTransform(-3,19.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.textetoutpromo, new cjs.Rectangle(-3,6.5,329,111.6), null);


(lib.sloganback = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.fondslogan();
	this.instance.parent = this;
	this.instance.setTransform(27,0,1.4184,1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sloganback, new cjs.Rectangle(27,0,817,87), null);


// stage content:
(lib.welcomeAnimate = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// pointexclam
	this.instance = new lib.CachedTexturedBitmap_1();
	this.instance.parent = this;
	this.instance.setTransform(867.9,108.05,0.5,0.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(74).to({_off:false},0).wait(1));

	// s
	this.instance_1 = new lib.CachedTexturedBitmap_2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(803.9,111.05,0.5,0.5);

	this.instance_2 = new lib.CachedTexturedBitmap_3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(848.9,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_1}]},73).to({state:[{t:this.instance_2}]},1).wait(1));

	// t
	this.instance_3 = new lib.CachedTexturedBitmap_4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(789.9,112.05,0.5,0.5);

	this.instance_4 = new lib.CachedTexturedBitmap_5();
	this.instance_4.parent = this;
	this.instance_4.setTransform(833.9,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3}]},72).to({state:[{t:this.instance_4}]},2).wait(1));

	// r
	this.instance_5 = new lib.CachedTexturedBitmap_6();
	this.instance_5.parent = this;
	this.instance_5.setTransform(774.9,112.05,0.5,0.5);

	this.instance_6 = new lib.CachedTexturedBitmap_7();
	this.instance_6.parent = this;
	this.instance_6.setTransform(817.9,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_5}]},71).to({state:[{t:this.instance_6}]},3).wait(1));

	// o
	this.instance_7 = new lib.CachedTexturedBitmap_8();
	this.instance_7.parent = this;
	this.instance_7.setTransform(752.9,112.05,0.5,0.5);

	this.instance_8 = new lib.CachedTexturedBitmap_9();
	this.instance_8.parent = this;
	this.instance_8.setTransform(796.9,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_7}]},70).to({state:[{t:this.instance_8}]},4).wait(1));

	// f
	this.instance_9 = new lib.CachedTexturedBitmap_10();
	this.instance_9.parent = this;
	this.instance_9.setTransform(738.9,111.05,0.5,0.5);

	this.instance_10 = new lib.CachedTexturedBitmap_11();
	this.instance_10.parent = this;
	this.instance_10.setTransform(783.9,109.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_9}]},69).to({state:[{t:this.instance_10}]},5).wait(1));

	// s
	this.instance_11 = new lib.CachedTexturedBitmap_12();
	this.instance_11.parent = this;
	this.instance_11.setTransform(710.9,111.05,0.5,0.5);

	this.instance_12 = new lib.CachedTexturedBitmap_13();
	this.instance_12.parent = this;
	this.instance_12.setTransform(754.9,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_11}]},68).to({state:[{t:this.instance_12}]},6).wait(1));

	// u
	this.instance_13 = new lib.CachedTexturedBitmap_14();
	this.instance_13.parent = this;
	this.instance_13.setTransform(687.9,111.05,0.5,0.5);

	this.instance_14 = new lib.CachedTexturedBitmap_15();
	this.instance_14.parent = this;
	this.instance_14.setTransform(733.9,109.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_13}]},67).to({state:[{t:this.instance_14}]},7).wait(1));

	// l
	this.instance_15 = new lib.CachedTexturedBitmap_16();
	this.instance_15.parent = this;
	this.instance_15.setTransform(675.9,111.05,0.5,0.5);

	this.instance_16 = new lib.CachedTexturedBitmap_17();
	this.instance_16.parent = this;
	this.instance_16.setTransform(723.9,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_15}]},66).to({state:[{t:this.instance_16}]},8).wait(1));

	// p
	this.instance_17 = new lib.CachedTexturedBitmap_18();
	this.instance_17.parent = this;
	this.instance_17.setTransform(653.9,105.05,0.5,0.5);

	this.instance_18 = new lib.CachedTexturedBitmap_19();
	this.instance_18.parent = this;
	this.instance_18.setTransform(701.9,103.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_17}]},65).to({state:[{t:this.instance_18}]},9).wait(1));

	// s
	this.instance_19 = new lib.CachedTexturedBitmap_20();
	this.instance_19.parent = this;
	this.instance_19.setTransform(624.9,109.05,0.5,0.5);

	this.instance_20 = new lib.CachedTexturedBitmap_21();
	this.instance_20.parent = this;
	this.instance_20.setTransform(673.9,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_19}]},64).to({state:[{t:this.instance_20}]},10).wait(1));

	// e
	this.instance_21 = new lib.CachedTexturedBitmap_22();
	this.instance_21.parent = this;
	this.instance_21.setTransform(603.9,109.05,0.5,0.5);

	this.instance_22 = new lib.CachedTexturedBitmap_23();
	this.instance_22.parent = this;
	this.instance_22.setTransform(654.9,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_21}]},63).to({state:[{t:this.instance_22}]},11).wait(1));

	// m
	this.instance_23 = new lib.CachedTexturedBitmap_24();
	this.instance_23.parent = this;
	this.instance_23.setTransform(573.95,110.05,0.5,0.5);

	this.instance_24 = new lib.CachedTexturedBitmap_25();
	this.instance_24.parent = this;
	this.instance_24.setTransform(624.95,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_23}]},62).to({state:[{t:this.instance_24}]},12).wait(1));

	// m
	this.instance_25 = new lib.CachedTexturedBitmap_26();
	this.instance_25.parent = this;
	this.instance_25.setTransform(542.95,110.05,0.5,0.5);

	this.instance_26 = new lib.CachedTexturedBitmap_27();
	this.instance_26.parent = this;
	this.instance_26.setTransform(593.95,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_25}]},61).to({state:[{t:this.instance_26}]},13).wait(1));

	// o
	this.instance_27 = new lib.CachedTexturedBitmap_28();
	this.instance_27.parent = this;
	this.instance_27.setTransform(521.95,111.05,0.5,0.5);

	this.instance_28 = new lib.CachedTexturedBitmap_29();
	this.instance_28.parent = this;
	this.instance_28.setTransform(573.95,109.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_27}]},60).to({state:[{t:this.instance_28}]},14).wait(1));

	// s
	this.instance_29 = new lib.CachedTexturedBitmap_30();
	this.instance_29.parent = this;
	this.instance_29.setTransform(503.95,111.05,0.5,0.5);

	this.instance_30 = new lib.CachedTexturedBitmap_31();
	this.instance_30.parent = this;
	this.instance_30.setTransform(554.95,109.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_29}]},59).to({state:[{t:this.instance_30}]},15).wait(1));

	// s
	this.instance_31 = new lib.CachedTexturedBitmap_32();
	this.instance_31.parent = this;
	this.instance_31.setTransform(476.9,112.05,0.5,0.5);

	this.instance_32 = new lib.CachedTexturedBitmap_33();
	this.instance_32.parent = this;
	this.instance_32.setTransform(527.9,109.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_31}]},58).to({state:[{t:this.instance_32}]},16).wait(1));

	// u
	this.instance_33 = new lib.CachedTexturedBitmap_34();
	this.instance_33.parent = this;
	this.instance_33.setTransform(455.95,114.05,0.5,0.5);

	this.instance_34 = new lib.CachedTexturedBitmap_35();
	this.instance_34.parent = this;
	this.instance_34.setTransform(455.95,112.05,0.5,0.5);

	this.instance_35 = new lib.CachedTexturedBitmap_36();
	this.instance_35.parent = this;
	this.instance_35.setTransform(506.95,109.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_33}]},57).to({state:[{t:this.instance_34}]},1).to({state:[{t:this.instance_35}]},16).wait(1));

	// o
	this.instance_36 = new lib.CachedTexturedBitmap_37();
	this.instance_36.parent = this;
	this.instance_36.setTransform(435.95,113.05,0.5,0.5);

	this.instance_37 = new lib.CachedTexturedBitmap_38();
	this.instance_37.parent = this;
	this.instance_37.setTransform(435.95,113.05,0.5,0.5);

	this.instance_38 = new lib.CachedTexturedBitmap_39();
	this.instance_38.parent = this;
	this.instance_38.setTransform(435.95,113.05,0.5,0.5);

	this.instance_39 = new lib.CachedTexturedBitmap_40();
	this.instance_39.parent = this;
	this.instance_39.setTransform(486.95,109.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_36}]},56).to({state:[{t:this.instance_37}]},1).to({state:[{t:this.instance_38}]},1).to({state:[{t:this.instance_39}]},16).wait(1));

	// n
	this.instance_40 = new lib.CachedTexturedBitmap_41();
	this.instance_40.parent = this;
	this.instance_40.setTransform(414.95,112.05,0.5,0.5);

	this.instance_41 = new lib.CachedTexturedBitmap_42();
	this.instance_41.parent = this;
	this.instance_41.setTransform(414.95,112.05,0.5,0.5);

	this.instance_42 = new lib.CachedTexturedBitmap_43();
	this.instance_42.parent = this;
	this.instance_42.setTransform(414.95,112.05,0.5,0.5);

	this.instance_43 = new lib.CachedTexturedBitmap_44();
	this.instance_43.parent = this;
	this.instance_43.setTransform(414.95,112.05,0.5,0.5);

	this.instance_44 = new lib.CachedTexturedBitmap_45();
	this.instance_44.parent = this;
	this.instance_44.setTransform(465.95,109.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_40}]},55).to({state:[{t:this.instance_41}]},1).to({state:[{t:this.instance_42}]},1).to({state:[{t:this.instance_43}]},1).to({state:[{t:this.instance_44}]},16).wait(1));

	// virgule
	this.instance_45 = new lib.CachedTexturedBitmap_46();
	this.instance_45.parent = this;
	this.instance_45.setTransform(401.95,109.05,0.5,0.5);

	this.instance_46 = new lib.CachedTexturedBitmap_47();
	this.instance_46.parent = this;
	this.instance_46.setTransform(401.95,109.05,0.5,0.5);

	this.instance_47 = new lib.CachedTexturedBitmap_48();
	this.instance_47.parent = this;
	this.instance_47.setTransform(401.95,109.05,0.5,0.5);

	this.instance_48 = new lib.CachedTexturedBitmap_49();
	this.instance_48.parent = this;
	this.instance_48.setTransform(401.95,109.05,0.5,0.5);

	this.instance_49 = new lib.CachedTexturedBitmap_50();
	this.instance_49.parent = this;
	this.instance_49.setTransform(401.95,109.05,0.5,0.5);

	this.instance_50 = new lib.CachedTexturedBitmap_51();
	this.instance_50.parent = this;
	this.instance_50.setTransform(452.95,106.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_45}]},54).to({state:[{t:this.instance_46}]},1).to({state:[{t:this.instance_47}]},1).to({state:[{t:this.instance_48}]},1).to({state:[{t:this.instance_49}]},1).to({state:[{t:this.instance_50}]},16).wait(1));

	// s
	this.instance_51 = new lib.CachedTexturedBitmap_52();
	this.instance_51.parent = this;
	this.instance_51.setTransform(382.9,111.05,0.5,0.5);

	this.instance_52 = new lib.CachedTexturedBitmap_53();
	this.instance_52.parent = this;
	this.instance_52.setTransform(382.9,111.05,0.5,0.5);

	this.instance_53 = new lib.CachedTexturedBitmap_54();
	this.instance_53.parent = this;
	this.instance_53.setTransform(382.9,111.05,0.5,0.5);

	this.instance_54 = new lib.CachedTexturedBitmap_55();
	this.instance_54.parent = this;
	this.instance_54.setTransform(382.9,111.05,0.5,0.5);

	this.instance_55 = new lib.CachedTexturedBitmap_56();
	this.instance_55.parent = this;
	this.instance_55.setTransform(382.9,111.05,0.5,0.5);

	this.instance_56 = new lib.CachedTexturedBitmap_57();
	this.instance_56.parent = this;
	this.instance_56.setTransform(382.9,111.05,0.5,0.5);

	this.instance_57 = new lib.CachedTexturedBitmap_58();
	this.instance_57.parent = this;
	this.instance_57.setTransform(433.9,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_51}]},53).to({state:[{t:this.instance_52}]},1).to({state:[{t:this.instance_53}]},1).to({state:[{t:this.instance_54}]},1).to({state:[{t:this.instance_55}]},1).to({state:[{t:this.instance_56}]},1).to({state:[{t:this.instance_57}]},16).wait(1));

	// i
	this.instance_58 = new lib.CachedTexturedBitmap_59();
	this.instance_58.parent = this;
	this.instance_58.setTransform(374.95,111.05,0.5,0.5);

	this.instance_59 = new lib.CachedTexturedBitmap_60();
	this.instance_59.parent = this;
	this.instance_59.setTransform(374.95,111.05,0.5,0.5);

	this.instance_60 = new lib.CachedTexturedBitmap_61();
	this.instance_60.parent = this;
	this.instance_60.setTransform(374.95,111.05,0.5,0.5);

	this.instance_61 = new lib.CachedTexturedBitmap_62();
	this.instance_61.parent = this;
	this.instance_61.setTransform(374.95,111.05,0.5,0.5);

	this.instance_62 = new lib.CachedTexturedBitmap_63();
	this.instance_62.parent = this;
	this.instance_62.setTransform(374.95,111.05,0.5,0.5);

	this.instance_63 = new lib.CachedTexturedBitmap_64();
	this.instance_63.parent = this;
	this.instance_63.setTransform(374.95,111.05,0.5,0.5);

	this.instance_64 = new lib.CachedTexturedBitmap_65();
	this.instance_64.parent = this;
	this.instance_64.setTransform(374.95,111.05,0.5,0.5);

	this.instance_65 = new lib.CachedTexturedBitmap_66();
	this.instance_65.parent = this;
	this.instance_65.setTransform(425.95,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_58}]},52).to({state:[{t:this.instance_59}]},1).to({state:[{t:this.instance_60}]},1).to({state:[{t:this.instance_61}]},1).to({state:[{t:this.instance_62}]},1).to({state:[{t:this.instance_63}]},1).to({state:[{t:this.instance_64}]},1).to({state:[{t:this.instance_65}]},16).wait(1));

	// n
	this.instance_66 = new lib.CachedTexturedBitmap_67();
	this.instance_66.parent = this;
	this.instance_66.setTransform(356.95,110.05,0.5,0.5);

	this.instance_67 = new lib.CachedTexturedBitmap_68();
	this.instance_67.parent = this;
	this.instance_67.setTransform(356.95,110.05,0.5,0.5);

	this.instance_68 = new lib.CachedTexturedBitmap_69();
	this.instance_68.parent = this;
	this.instance_68.setTransform(356.95,110.05,0.5,0.5);

	this.instance_69 = new lib.CachedTexturedBitmap_70();
	this.instance_69.parent = this;
	this.instance_69.setTransform(356.95,110.05,0.5,0.5);

	this.instance_70 = new lib.CachedTexturedBitmap_71();
	this.instance_70.parent = this;
	this.instance_70.setTransform(356.95,110.05,0.5,0.5);

	this.instance_71 = new lib.CachedTexturedBitmap_72();
	this.instance_71.parent = this;
	this.instance_71.setTransform(356.95,110.05,0.5,0.5);

	this.instance_72 = new lib.CachedTexturedBitmap_73();
	this.instance_72.parent = this;
	this.instance_72.setTransform(356.95,110.05,0.5,0.5);

	this.instance_73 = new lib.CachedTexturedBitmap_74();
	this.instance_73.parent = this;
	this.instance_73.setTransform(356.95,110.05,0.5,0.5);

	this.instance_74 = new lib.CachedTexturedBitmap_75();
	this.instance_74.parent = this;
	this.instance_74.setTransform(407.95,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_66}]},51).to({state:[{t:this.instance_67}]},1).to({state:[{t:this.instance_68}]},1).to({state:[{t:this.instance_69}]},1).to({state:[{t:this.instance_70}]},1).to({state:[{t:this.instance_71}]},1).to({state:[{t:this.instance_72}]},1).to({state:[{t:this.instance_73}]},1).to({state:[{t:this.instance_74}]},16).wait(1));

	// U
	this.instance_75 = new lib.CachedTexturedBitmap_76();
	this.instance_75.parent = this;
	this.instance_75.setTransform(333.95,111.05,0.5,0.5);

	this.instance_76 = new lib.CachedTexturedBitmap_77();
	this.instance_76.parent = this;
	this.instance_76.setTransform(333.95,111.05,0.5,0.5);

	this.instance_77 = new lib.CachedTexturedBitmap_78();
	this.instance_77.parent = this;
	this.instance_77.setTransform(333.95,111.05,0.5,0.5);

	this.instance_78 = new lib.CachedTexturedBitmap_79();
	this.instance_78.parent = this;
	this.instance_78.setTransform(333.95,111.05,0.5,0.5);

	this.instance_79 = new lib.CachedTexturedBitmap_80();
	this.instance_79.parent = this;
	this.instance_79.setTransform(333.95,111.05,0.5,0.5);

	this.instance_80 = new lib.CachedTexturedBitmap_81();
	this.instance_80.parent = this;
	this.instance_80.setTransform(333.95,111.05,0.5,0.5);

	this.instance_81 = new lib.CachedTexturedBitmap_82();
	this.instance_81.parent = this;
	this.instance_81.setTransform(333.95,111.05,0.5,0.5);

	this.instance_82 = new lib.CachedTexturedBitmap_83();
	this.instance_82.parent = this;
	this.instance_82.setTransform(333.95,111.05,0.5,0.5);

	this.instance_83 = new lib.CachedTexturedBitmap_84();
	this.instance_83.parent = this;
	this.instance_83.setTransform(333.95,111.05,0.5,0.5);

	this.instance_84 = new lib.CachedTexturedBitmap_85();
	this.instance_84.parent = this;
	this.instance_84.setTransform(384.95,108.05,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_75}]},50).to({state:[{t:this.instance_76}]},1).to({state:[{t:this.instance_77}]},1).to({state:[{t:this.instance_78}]},1).to({state:[{t:this.instance_79}]},1).to({state:[{t:this.instance_80}]},1).to({state:[{t:this.instance_81}]},1).to({state:[{t:this.instance_82}]},1).to({state:[{t:this.instance_83}]},1).to({state:[{t:this.instance_84}]},16).wait(1));

	// fondslogan
	this.instance_85 = new lib.sloganback();
	this.instance_85.parent = this;
	this.instance_85.setTransform(533.95,365.5,0.9963,1,0,0,0,287.9,43.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_85).wait(49).to({x:490.95,y:147.5},0).wait(26));

	// shirt1
	this.instance_86 = new lib.shirt();
	this.instance_86.parent = this;
	this.instance_86.setTransform(268,306);

	this.timeline.addTween(cjs.Tween.get(this.instance_86).wait(46).to({x:183,y:22},0).wait(29));

	// bluehouse1
	this.instance_87 = new lib.bluehouse();
	this.instance_87.parent = this;
	this.instance_87.setTransform(1264,366);

	this.instance_88 = new lib.bluehouse();
	this.instance_88.parent = this;
	this.instance_88.setTransform(1078,146);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_87,p:{x:1264,y:366}}]}).to({state:[{t:this.instance_87,p:{x:1078,y:146}}]},42).to({state:[{t:this.instance_88},{t:this.instance_87,p:{x:768,y:0}}]},7).to({state:[{t:this.instance_88},{t:this.instance_87,p:{x:768,y:0}}]},4).to({state:[{t:this.instance_88},{t:this.instance_87,p:{x:768,y:0}}]},21).wait(1));

	// house1
	this.instance_89 = new lib.house();
	this.instance_89.parent = this;
	this.instance_89.setTransform(1053,361);

	this.timeline.addTween(cjs.Tween.get(this.instance_89).wait(37).to({x:825,y:118},0).wait(12).to({x:629,y:163},0).wait(26));

	// bot1
	this.instance_90 = new lib.bot();
	this.instance_90.parent = this;
	this.instance_90.setTransform(1243,416);

	this.timeline.addTween(cjs.Tween.get(this.instance_90).wait(32).to({x:735,y:148},0).wait(17).to({x:1148,y:189},0).wait(26));

	// trousers1
	this.instance_91 = new lib.trousers();
	this.instance_91.parent = this;
	this.instance_91.setTransform(-100,-89);

	this.timeline.addTween(cjs.Tween.get(this.instance_91).wait(27).to({x:129,y:118},0).wait(22).to({x:22,y:26},0).wait(26));

	// yellowphone
	this.instance_92 = new lib.yellowphone();
	this.instance_92.parent = this;
	this.instance_92.setTransform(283,366);

	this.instance_93 = new lib.yellowphone();
	this.instance_93.parent = this;
	this.instance_93.setTransform(141,127);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_92,p:{x:283,y:366}}]}).to({state:[{t:this.instance_92,p:{x:141,y:127}}]},42).to({state:[{t:this.instance_93},{t:this.instance_92,p:{x:274,y:-38}}]},7).to({state:[{t:this.instance_93},{t:this.instance_92,p:{x:274,y:-38}}]},4).to({state:[{t:this.instance_93},{t:this.instance_92,p:{x:274,y:-38}}]},21).wait(1));

	// trousers
	this.instance_94 = new lib.trousers();
	this.instance_94.parent = this;
	this.instance_94.setTransform(-90,226);

	this.timeline.addTween(cjs.Tween.get(this.instance_94).wait(22).to({x:972,y:9},0).wait(27).to({x:1009,y:7},0).wait(26));

	// t_shirt
	this.instance_95 = new lib.tshirt();
	this.instance_95.parent = this;
	this.instance_95.setTransform(8,310);

	this.timeline.addTween(cjs.Tween.get(this.instance_95).wait(17).to({x:420,y:140},0).wait(32).to({x:1229,y:182},0).wait(26));

	// shoppingcart
	this.instance_96 = new lib.shoppingcart();
	this.instance_96.parent = this;
	this.instance_96.setTransform(49,351);

	this.timeline.addTween(cjs.Tween.get(this.instance_96).wait(12).to({x:955,y:127},0).wait(37).to({x:768,y:174},0).wait(26));

	// shirt
	this.instance_97 = new lib.shirt();
	this.instance_97.parent = this;
	this.instance_97.setTransform(187,380);

	this.instance_98 = new lib.shirt();
	this.instance_98.parent = this;
	this.instance_98.setTransform(1065,44);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_97,p:{x:187,y:380}}]}).to({state:[{t:this.instance_98},{t:this.instance_97,p:{x:889,y:-13}}]},49).to({state:[{t:this.instance_98},{t:this.instance_97,p:{x:889,y:-13}}]},4).to({state:[{t:this.instance_98},{t:this.instance_97,p:{x:889,y:-13}}]},21).wait(1));

	// sandal
	this.instance_99 = new lib.sandal();
	this.instance_99.parent = this;
	this.instance_99.setTransform(-52,366);

	this.timeline.addTween(cjs.Tween.get(this.instance_99).wait(44).to({x:650,y:13},0).wait(31));

	// player
	this.instance_100 = new lib.player();
	this.instance_100.parent = this;
	this.instance_100.setTransform(529,326);

	this.timeline.addTween(cjs.Tween.get(this.instance_100).wait(39).to({x:1182,y:7},0).wait(36));

	// car
	this.instance_101 = new lib.car();
	this.instance_101.parent = this;
	this.instance_101.setTransform(108,310);

	this.timeline.addTween(cjs.Tween.get(this.instance_101).wait(34).to({x:644,y:132},0).wait(15).to({x:427,y:184},0).wait(26));

	// bot
	this.instance_102 = new lib.bot();
	this.instance_102.parent = this;
	this.instance_102.setTransform(-79,281);

	this.timeline.addTween(cjs.Tween.get(this.instance_102).wait(29).to({x:1332,y:7},0).wait(46));

	// bluehouse
	this.instance_103 = new lib.bluehouse();
	this.instance_103.parent = this;
	this.instance_103.setTransform(283,-98);

	this.timeline.addTween(cjs.Tween.get(this.instance_103).wait(24).to({y:136},0).wait(25).to({x:89,y:204},0).wait(26));

	// bluecar
	this.instance_104 = new lib.bluecar();
	this.instance_104.parent = this;
	this.instance_104.setTransform(-87,210);

	this.timeline.addTween(cjs.Tween.get(this.instance_104).wait(19).to({x:1182,y:136},0).wait(56));

	// blackphone
	this.instance_105 = new lib.blackphone();
	this.instance_105.parent = this;
	this.instance_105.setTransform(378,-79);

	this.timeline.addTween(cjs.Tween.get(this.instance_105).wait(14).to({x:1306,y:118},0).wait(61));

	// house
	this.instance_106 = new lib.house();
	this.instance_106.parent = this;
	this.instance_106.setTransform(-190,209);

	this.timeline.addTween(cjs.Tween.get(this.instance_106).wait(9).to({x:89,y:0},0).wait(66));

	// bag
	this.instance_107 = new lib.bag();
	this.instance_107.parent = this;
	this.instance_107.setTransform(140,-79);

	this.instance_108 = new lib.bag();
	this.instance_108.parent = this;
	this.instance_108.setTransform(32,149);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_107,p:{x:140,y:-79}}]}).to({state:[{t:this.instance_107,p:{x:33,y:149}}]},1).to({state:[{t:this.instance_108},{t:this.instance_107,p:{x:427,y:-20}}]},48).to({state:[{t:this.instance_108},{t:this.instance_107,p:{x:427,y:-20}}]},4).to({state:[{t:this.instance_108},{t:this.instance_107,p:{x:427,y:-20}}]},21).wait(1));

	// textebienvenue
	this.instance_109 = new lib.textebienvenue1();
	this.instance_109.parent = this;
	this.instance_109.setTransform(-256.3,49.6,1,1,0,0,0,212.7,55.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_109).wait(1).to({regX:203.4,regY:69,x:-250.2,y:63.4},0).wait(1).to({x:-234.85,y:63.7},0).wait(1).to({x:-219.45,y:64},0).wait(1).to({x:-204.1,y:64.25},0).wait(1).to({x:-188.75,y:64.55},0).wait(1).to({x:-173.35,y:64.85},0).wait(1).to({x:-158,y:65.1},0).wait(1).to({x:-142.65,y:65.4},0).wait(1).to({x:-127.25,y:65.7},0).wait(1).to({x:-111.9,y:65.95},0).wait(1).to({x:-96.55,y:66.25},0).wait(1).to({x:-81.15,y:66.55},0).wait(1).to({x:-65.8,y:66.8},0).wait(1).to({x:-50.45,y:67.1},0).wait(1).to({x:-35.05,y:67.4},0).wait(1).to({x:-19.7,y:67.65},0).wait(1).to({x:-4.35,y:67.95},0).wait(1).to({x:11.05,y:68.25},0).wait(1).to({x:26.4,y:68.5},0).wait(1).to({x:41.75,y:68.8},0).wait(1).to({x:57.15,y:69.05},0).wait(1).to({x:72.5,y:69.3},0).wait(1).to({x:87.85,y:69.6},0).wait(1).to({x:103.25,y:69.9},0).wait(1).to({x:118.6,y:70.15},0).wait(1).to({x:134,y:70.45},0).wait(1).to({x:149.35,y:70.75},0).wait(1).to({x:164.7,y:71},0).wait(1).to({x:180.1,y:71.3},0).wait(1).to({x:195.45,y:71.6},0).wait(1).to({x:210.75,y:71.85},0).wait(1).to({x:226.15,y:72.15},0).wait(1).to({x:241.5,y:72.45},0).wait(1).to({x:256.85,y:72.7},0).wait(1).to({x:272.25,y:73},0).wait(1).to({x:287.6,y:73.3},0).wait(1).to({x:302.95,y:73.55},0).wait(1).to({x:318.35,y:73.85},0).wait(1).to({x:333.7,y:74.15},0).wait(1).to({x:349.05,y:74.4},0).wait(1).to({x:364.45,y:74.7},0).wait(1).to({x:379.8,y:75},0).wait(1).to({x:395.15,y:75.25},0).wait(1).to({x:410.55,y:75.55},0).wait(1).to({x:425.9,y:75.85},0).wait(1).to({x:441.25,y:76.1},0).wait(1).to({x:456.65,y:76.4},0).wait(1).to({x:472,y:76.7},0).wait(1).to({x:487.4,y:77},0).wait(26));

	// texte2
	this.instance_110 = new lib.textetoutpromo();
	this.instance_110.parent = this;
	this.instance_110.setTransform(1533.6,54.95,1,1,0,0,0,165.7,47);

	this.timeline.addTween(cjs.Tween.get(this.instance_110).wait(1).to({regX:161.5,regY:62.3,x:1515.4,y:70.4},0).wait(1).to({x:1501.4,y:70.55},0).wait(1).to({x:1487.4,y:70.7},0).wait(1).to({x:1473.4,y:70.85},0).wait(1).to({x:1459.4,y:71},0).wait(1).to({x:1445.4,y:71.15},0).wait(1).to({x:1431.4,y:71.3},0).wait(1).to({x:1417.4,y:71.45},0).wait(1).to({x:1403.4,y:71.6},0).wait(1).to({x:1389.4,y:71.75},0).wait(1).to({x:1375.4,y:71.9},0).wait(1).to({x:1361.4,y:72.05},0).wait(1).to({x:1347.4,y:72.25},0).wait(1).to({x:1333.4,y:72.4},0).wait(1).to({x:1319.4,y:72.55},0).wait(1).to({x:1305.4,y:72.7},0).wait(1).to({x:1291.4,y:72.85},0).wait(1).to({x:1277.4,y:73},0).wait(1).to({x:1263.4,y:73.15},0).wait(1).to({x:1249.4,y:73.3},0).wait(1).to({x:1235.4,y:73.45},0).wait(1).to({x:1221.4,y:73.6},0).wait(1).to({x:1207.4,y:73.75},0).wait(1).to({x:1193.4,y:73.9},0).wait(1).to({x:1179.45,y:74.1},0).wait(1).to({x:1165.45,y:74.25},0).wait(1).to({x:1151.45,y:74.4},0).wait(1).to({x:1137.45,y:74.55},0).wait(1).to({x:1123.45,y:74.7},0).wait(1).to({x:1109.45,y:74.85},0).wait(1).to({x:1095.45,y:75},0).wait(1).to({x:1081.45,y:75.15},0).wait(1).to({x:1067.45,y:75.3},0).wait(1).to({x:1053.45,y:75.45},0).wait(1).to({x:1039.45,y:75.6},0).wait(1).to({x:1025.45,y:75.75},0).wait(1).to({x:1011.45,y:75.95},0).wait(1).to({x:997.45,y:76.1},0).wait(1).to({x:983.45,y:76.25},0).wait(1).to({x:969.45,y:76.4},0).wait(1).to({x:955.45,y:76.55},0).wait(1).to({x:941.45,y:76.7},0).wait(1).to({x:927.45,y:76.85},0).wait(1).to({x:913.45,y:77},0).wait(1).to({x:899.45,y:77.15},0).wait(1).to({x:885.45,y:77.3},0).wait(1).to({x:871.45,y:77.45},0).wait(1).to({x:857.45,y:77.6},0).wait(1).to({x:843.5,y:77.8},0).wait(26));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(203.7,27,1490.2,430);
// library properties:
lib.properties = {
	id: '94CA2E36C6F7664DB4B2D5AFF2FD4ABE',
	width: 1366,
	height: 250,
	fps: 24,
	color: "#000033",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/animateWelcome_atlas_.png?1571386840604", id:"animateWelcome_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['94CA2E36C6F7664DB4B2D5AFF2FD4ABE'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;
